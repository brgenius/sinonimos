class ApplicationController
	@@active_user = nil
	@current_page = nil
	@views = 0 

	def initialize
		@current_page = 'no page yet'
	end

	def active_user
		@@active_user
	end

	def set_user( user )
		@@active_user = user
	end

	def who_iam()
		self.active_user
	end

	def visited_page()
		@views = @views + 1
	end

	def current_page
		"#{ @views } at #{ @current_page }"
	end

end
 
class LoginController < ApplicationController

	def initialize
		@current_page = 'Login page'
		@views = 0
	end

	def login(user)
		set_user( user )
	end

end

class HomeController < ApplicationController

	def initialize
		@current_page = 'Home page'
		@views = 0
	end

end

login_controller =  LoginController.new()
login_controller.login("bernardo")
login_controller.visited_page

puts "#{ login_controller.who_iam } is at #{ login_controller.current_page }"

home_controller = HomeController.new()
puts "#{ home_controller.who_iam } is at #{ home_controller.current_page }"

3.times{|x|  home_controller.visited_page }

puts "#{ home_controller.who_iam } is at #{ home_controller.current_page }"
puts "#{ login_controller.who_iam } is at #{ login_controller.current_page }"