
$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'app'))
$LOAD_PATH.unshift(File.dirname(__FILE__))

['../../app/**/*.rb' ].each do |path|
  Dir[File.expand_path(path, __FILE__)].each do |f|
    require f
  end
end

