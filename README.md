Setup guide
===================


Hey! This is *Bernardo Bucher*'s solution for **question 5**. In this document you will learn how to setup and execute it.

----------

Instalation
-------------

First, you'll need ruby **ruby 2.1.1p76**. To install using ruby using RVM: 

> **Install rvm:**

> - $ gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3

> - $ \curl -sSL https://get.rvm.io | bash -s stable


> **Install ruby:**
> $ rvm install 2.1.1

With ruby instaled you must install rspec for run the tests

> **Install rspec:**
> $ gem install rspec

Then, inside the folder run:

> $ bundle install

----------

Run the tests
-------------


> **Just run:**
> $ rspec

You should see somenthing like:

......

Finished in 0.06868 seconds (files took 0.14285 seconds to load)
6 examples, 0 failures


----------

Runing the application
-------------

Just run:
> $ ruby app.rb 

Or, passing your own files

> $ ruby app.rb Sinonimos.json strings.txt
