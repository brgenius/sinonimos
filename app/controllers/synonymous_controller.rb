require "utf8_utils"

class SynonymousController < AppController

	attr_accessor :synonymous
	def initialize( synonymous_file )
		input_helper = InputHelper.new synonymous_file
		@synonymous = input_helper.read_json
	end

	def process(string_file , output_file = 'output.txt')
		input_helper = InputHelper.new string_file
		array = input_helper.read_array.map{|x| 
			begin
				x.tidy_bytes.sub("\n", '').strip 	
			rescue Exception => e	
				x.scrub.sub("\n", '').strip 	
			end			
		}

		result = array

		result.each_with_index do |s , i|
			syn = @synonymous[s]

			next if syn == nil

			result[i] = syn["Chave"]
		end
		
		save_file(result , output_file)
		result
	end

	private 

	def save_file(strs, output_file)
		File.open(output_file, 'w') { |file| file.write( strs.join("\n") ) }
	end



end