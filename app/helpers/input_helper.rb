require "json"

class InputHelper

	attr_accessor :file_name

	def initialize(file_name)
		@file_name = file_name
	end

	def read
		File.read(file_name)
	end

	def read_json
		file = read
		JSON.parse(file)
	end

	def read_array
		IO.readlines(file_name)
		#File.readlines(file_name, :encoding => 'UTF-8')
	end

end