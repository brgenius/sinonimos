require 'spec_helper'
require 'tempfile'

describe 'SynonymousController' do 
		
	context 'Reads an string file, and outputs a new one replacing the synonymous' do

		before do
			file =  '{ "1080p": {"Chave": "fullhd", "Nome": "1080p", "Idioma": "pt" }, ' + 
			       	'  "3d modeling": {"Chave": "3dmodel","Nome": "3d modeling","Idioma": "en" }, '+
					'  "3d printing": {"Chave": "3dprint","Nome": "3d printing", "Idioma": "en" }, ' +
					'  "3d scanning": {"Chave": "3dscan","Nome": "3d scanning","Idioma": "en" }, ' +
					'  "à prova de bala": {"Chave": "bulletproof", "Nome": "à prova de bala", "Idioma": "pt" }, '  +
					'  "relaxado": {"Chave": "relax", "Nome": "relaxado", "Idioma": "pt" } }'

    		@synonymous_file = Tempfile.open( 'sinonimos_temp.json' )
    		@synonymous_file.write file
    		@synonymous_file.close

    		
    		file_str =  "pintada à mão\n" +
						"1080p\n" +
						"Baby Dolls\n" +
						"à prova de bala\n"+
						"capa de chave\n"+
						"relaxado\n" +
						"remote controlled car\n" +
						"crescimento\n" +
						"porta\n" +
						"sweatshirt\n" +
						"Brica\n" +
						"elevator\n" +
						"chuteira\n" +
						"Pneu\n" +
						"seasonal\n" +
						"3d modeling\n" 

    		@strings_temp = Tempfile.open( 'strings_temp.txt' )
    		@strings_temp.write file_str
    		@strings_temp.close


		end

		it 'Should be able to replace all the elements from a string file with the loaded synonymous' do
    		
			synonymous_controller = SynonymousController.new( @synonymous_file.path )
			result = synonymous_controller.process(@strings_temp)

			file_str = ["pintada à mão" ,
						"fullhd" ,
						"Baby Dolls" ,
						"bulletproof" ,
						"capa de chave" ,
						"relax" , 
						"remote controlled car"  ,
						"crescimento"  ,
						"porta"  , 
						"sweatshirt" , 
						"Brica"  , 
						"elevator" , 
						"chuteira" ,
						"Pneu" , 
						"seasonal" , 
						"3dmodel" ]

			expect(result).to match_array(file_str)  
			
		end

		it 'Should save the output to a new file with the expected strings' do
			synonymous_controller = SynonymousController.new( @synonymous_file.path )
			result = synonymous_controller.process(@strings_temp, 'output.txt')

			saved_file = IO.readlines('output.txt')

			expect(result).to match_array(result)
		end

	end

end