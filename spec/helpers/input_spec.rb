require 'spec_helper'
require 'tempfile'
require 'json'

describe 'InputHelper' do 
	
	context 'Reads an input file' do

		before do
			@file = '{"1080p": {"Chave": "fullhd", "Nome": "1080p", "Idioma": "pt" }}'

    		@tempfile = Tempfile.open('sinonimos_temp.json')
    		@tempfile.write @file
    		@tempfile.close

		end

		it 'Should read an inputed file' do
			input_helper = InputHelper.new @tempfile.path
			list = input_helper.read
			
			expect(list).not_to be_empty
		end 

		it 'The read file should have the same contents of the original one' do
			input_helper = InputHelper.new @tempfile.path
			list = input_helper.read
			
			expect(list).to eq(@file)
		end

		it 'Should convert to a hash, when reading from a json' do
			input_helper = InputHelper.new @tempfile.path
			json = input_helper.read_json

			h = JSON.parse(@file)

			expect(h).to eq(json)
		end

		it 'Should convert the file lines to an string array' do 
			input_helper = InputHelper.new @tempfile.path

			list = input_helper.read_array

			expect([@file]).to match_array(list)
		end

	end

end