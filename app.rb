require_relative "config/application"

def main
	if ARGV[0] == nil 
	  synoymous_file = 'Sinonimos.json' 
	else
	  synoymous_file = ARGV[0] 
	end

	if ARGV[1] == nil 
	  string_file = 'strings.txt' 
	else
	  string_file = ARGV[1] 
	end

	synoymous = SynonymousController.new(synoymous_file)

	synoymous.process(string_file)
end

main()
